<?php
class Node {
    public $data = NULL;
    public $children = [];
    public function __construct(string $data = NULL) {
        $this->data = $data;
    }
    public function addChildren(Node $node) {
        $this->children[] = $node;
    }
}
class Tree {
    public $root = NULL;
    private SplQueue $visited;

    public function __construct(Node $node) {
        $this->root = $node;
        $this->visited = new SplQueue();
    }
    public function BFS(Node $node): SplQueue
    {
        $this->visited = new SplQueue(); // Reset visited queue
        $queue = new SplQueue();
        $queue->enqueue($node);
        while (!$queue->isEmpty()) {
            $current = $queue->dequeue();
            echo $current->data ."\n";             // visit node 1
            $this->visited->enqueue($current);     // store visited order
            foreach ($current->children as $child) {
                $queue->enqueue($child);            // enqueue next child 2,3,4
            }
        }

        return $this->visited;
    }

    public function DFS(Node $node): SplQueue
    {
        $this->visited = new SplQueue(); // Reset visited queue
        $this->visited->enqueue($node);
        echo $node->data ."\n";
        if($node->children){
            foreach ($node->children as $child) {
                $this->DFS($child);
            }
        }

        return $this->visited;
    }
}

$root = new Node("8");
$node2 = new Node("11");
$node1 = new Node("5");
$node3 = new Node("1");
$tree = new Tree($root);
$root->addChildren($node1);
$root->addChildren($node2);
$root->addChildren($node3);
$visited = $tree->BFS($tree->root);
$visited = $tree->DFS($tree->root);