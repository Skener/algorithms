
$arr = [
    ['id' => 100, 'parent_id' => 0, 'name' => 'a'],
    ['id' => 101, 'parent_id' => 100, 'name' => 'aa'],
    ['id' => 102, 'parent_id' => 101, 'name' => 'aaa'],
    ['id' => 103, 'parent_id' => 101, 'name' => 'aaaa'],
];

function buildTree($items)
{
    $child = [];
    foreach ($items as &$item) {
        $child[$item['parent_id']][] = &$item;
        unset($item);
    }
    foreach ($items as &$item) {
        if (isset($child[$item['id']])) {
            $item['childs'] = $child[$item['id']];
        }
    }
    return $child[0];
}

var_export(buildTree($arr));
