<?php
class MyIterator implements \Iterator
{
    public function __construct(
        private array $array=[],
        private int $position=0
    )
    {
        $this->count = count($this->array)-1;
    }

    public function current(): mixed
    {
       return $this->array[$this->position];
    }

    public function next(): void
    {
        $this->position++;
    }

    public function key(): int
    {
        return $this->position;
    }

    public function valid(): bool
    {
      return $this->position <= $this->count;
    }

    public function rewind(): void
    {
        $this->position = 0;
    }
}