<?php
// 
$ar = [1, 26, 3, 7, 4,  100, 0, 5];
$n = count($ar);
for ($i = 0; $i < $n; $i++) {
    // Last i elements are already in place 
    for ($j = 0; $j < $n - $i - 1; $j++) {
        if ($ar[$j] > $ar[$j + 1]) {
            // traverse the array from 0 to n-i-1 
            // Swap if the element found is greater 
            // than the next element 
            $temp = $ar[$j + 1];
            $ar[$j + 1] = $ar[$j];
            $ar[$j] = $temp;
        }
    }
}
var_dump ($ar);
