<?php

//Token bucket rate limiter
class TokenBucket {
    private $capacity;
    private $tokens;
    private $rate;
    private $lastTime;
    private $queue;

    public function __construct($capacity, $rate) {
        $this->capacity = $capacity;
        $this->tokens = $capacity;
        $this->rate = $rate;
        $this->lastTime = microtime(true);
        $this->queue = new SplQueue();
    }

    private function addTokens() {
        $now = microtime(true);
        $timePassed = $now - $this->lastTime;
        $this->lastTime = $now;

        $tokensToAdd = $timePassed * $this->rate;
        $this->tokens = min($this->capacity, $this->tokens + $tokensToAdd);
    }

    public function consume($tokens = 1) {
        $this->addTokens();

        if ($this->tokens >= $tokens) {
            $this->tokens -= $tokens;
            return true;
        }

        return false;
    }

    public function enqueueRequest($request) {
        $this->queue->enqueue($request);
    }

    public function processQueue() {
        while (!$this->queue->isEmpty()) {
            $request = $this->queue->dequeue();
            if ($this->consume()) {
                // Process the request
                echo "Processing request: $request\n";
            } else {
                // Re-queue the request if not enough tokens
                $this->queue->enqueue($request);
                break;
            }
        }
    }
}

// Example usage
$bucket = new TokenBucket(10, 1); // Capacity of 10 tokens, rate of 1 token per second

// Simulate incoming requests
for ($i = 1; $i <= 15; $i++) {
    $bucket->enqueueRequest("Request $i");
}

// Process the queue
while (true) {
    $bucket->processQueue();
    usleep(100000); // Sleep for 100ms to simulate time passing
}