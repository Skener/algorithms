<? php

class InvertedIndex {
    private $index = [];

    // Add a document to the index
    public function addDocument($docId, $content) {
        $words = preg_split('/\W+/', strtolower($content), -1, PREG_SPLIT_NO_EMPTY);

        foreach ($words as $word) {
            if (!isset($this->index[$word])) {
                $this->index[$word] = [];
            }

            if (!isset($this->index[$word][$docId])) {
                $this->index[$word][$docId] = 0;
            }

            $this->index[$word][$docId]++;
        }
    }


    // Search for a word in the index
    public function search($term) {
        $term = strtolower($term);

        return $this->index[$term] ?? [];
    }

    // Print the index
    public function printIndex() {
        foreach ($this->index as $word => $docData) {
            echo $word . ' => ';

            foreach ($docData as $docId => $frequency) {
                echo "[Doc $docId: $frequency] ";
            }

            echo "\n";
        }
    }

}

// Example usage
$invertedIndex = new InvertedIndex();

// Adding documents
$invertedIndex->addDocument(1, 'The quick brown fox jumps over the lazy dog'); // ["quick" => [1 => 1]]
$invertedIndex->addDocument(2, 'The quick blue cat jumps high');
$invertedIndex->addDocument(3, 'Blue and brown animals are common');

// Search for words
print_r($invertedIndex->search('quick')); // [1 => 1, 2 => 1]
print_r($invertedIndex->search('blue'));  // [2 => 1, 3 => 1]

// Print the entire index
$invertedIndex->printIndex();