<?php

function exponentialBackoff($maxRetries, $initialDelay, $maxDelay, $operation) {
    $retries = 0;
    $delay = $initialDelay;

    while ($retries < $maxRetries) {
        try {
            return $operation();
        } catch (Exception $e) {
            error_log("Operation failed: " . $e->getMessage());
            $retries++;

            if ($retries >= $maxRetries) {
                throw $e;
            }

            sleep($delay);
            $delay = min($maxDelay, $delay * 2);
            echo "Retries: {$retries}\n";
            echo "Delay: {$delay}\n";
        }
    }
}

function callExternalApi($maxRetries = 3, $initialDelay = 1, $maxDelay = 32) {
    return exponentialBackoff($maxRetries, $initialDelay, $maxDelay, function() {
        $response = file_get_contents('https://jsonplaceholder.typicode.com/post');
        if ($response === FALSE) {
            throw new Exception("API call failed");
        }

        return $response;
    });
}

try {
    $data = callExternalApi();
    echo "API call succeeded: $data";
} catch (Exception $e) {
    echo "API call failed after retries: " . $e->getMessage();
}