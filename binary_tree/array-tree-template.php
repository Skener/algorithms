<? php

$tree = [
  'value' => 'PHPTree',
  'children' => [
    0 => [
      'value' => 'is',
      'children' => [
        0 => [
          'value' => 'fast',
          'children' => [
            0 => [
              'value' => 'to',
            ],
            1 => [
              'value' => 'manipulate',
            ],
          ],
        ],
        1 => [
          'value' => 'and',
          'children' => [
            0 => [
              'value' => 'trees',
            ],
            1 => [
              'value' => 'data',
            ],
          ],
        ],
      ],
    ],
    1 => [
      'value' => 'a',
      'children' => [
        0 => [
          'value' => 'fun',
          'children' => [
            0 => [
              'value' => 'structure',
            ],
          ],
        ],
        1 => [
          'value' => 'library',
        ],
      ],
    ],
  ],
];