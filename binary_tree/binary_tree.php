<?php

class BinaryTree
{
    public $nodes = [];

    public function __construct(Array $nodes)
    {
        $this->nodes = $nodes;
    }

    public function traverse(int $num = 0, int $level = 0)
    {
        if (isset($this->nodes[$num])) {
            echo str_repeat("-", $level);
            echo $this->nodes[$num] . "\n";
            $this->traverse((2 * $num) + 1, $level + 1);
            $this->traverse(2 * ($num + 1), $level + 1);
        }
    }
}

$tree = new BinaryTree([2, 4, 3, 5, 6, 9]);
$res = $tree->traverse();
var_dump($tree->nodes);



// Create Binary Balanced tree from sorted array
class Node
{
    public function __construct($value)
    {
        $this->left = null;
        $this->right = null;
        $this->value = $value;
    }
}

function  createBinarySearchTree(array $array, int $start, int $end): Node
{
    if ($start>$end) return;

    $mid = intdiv($start + $end, 2);

    $node = new Node($array[$mid]);

    $node->left = createBinarySearchTree($array, $start, $mid-1);
    $node->right = createBinarySearchTree($array, $mid+1, $end);

    return $node;
}

$array = [1,2,3,4,5,6,7,8,9];

var_dump(createBinarySearchTree($array, 0, 8));