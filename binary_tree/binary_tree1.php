<?php

class Node
{
    public function __construct($value)
    {
        $this->left = null;
        $this->right = null;
        $this->value = $value;
    }
}

class BinaryTree
{
    public function __construct()
    {
        $this->root = null;
    }

    public function insert($value)
    {
        $newNode = new Node($value);
        if ($this->root === null) {
            $this->root = $newNode;
        } else {
            $currentNode = $this->root;
            while (true) {
                if ($value < $currentNode->value) {
                    //Left
                    if (!$currentNode->left) {
                        $currentNode->left = $newNode;
                        return $this;
                    }
                    $currentNode = $currentNode->left;
                } else {
                    //Right
                    if (!$currentNode->right) {
                        $currentNode->right = $newNode;
                        return $this;
                    }
                    $currentNode = $currentNode->right;
                }
            }
        }
        return $this;
    }

    public function find($value)
    {
        if (!$this->root) {
            return false;
        }
        $found=false;
        $currentNode = $this->root;
        while ($currentNode) {
            if ($value < $currentNode->value) {
                $currentNode = $currentNode->left;
            } elseif ($value > $currentNode->value) {
                $currentNode = $currentNode->right;
            } elseif ($currentNode->value === $value) {
                $found=true;
                return $currentNode->value;
            }
        }
        return null;
    }


    public function delete($value)
    {
        if (!$this->root) {
            return false;
        }
        $currentNode = $this->root;
        $parentNode = null;
        while ($currentNode) {
            if ($value < $currentNode->value) {
                $parentNode = $currentNode;
                $currentNode = $currentNode->left;
            } else {
                if ($value > $currentNode->value) {
                    $parentNode = $currentNode;
                    $currentNode = $currentNode->right;
                } else {
                    if ($currentNode->value === $value) {
                        //We have a match, get to work!

                        //Option 1: No right child:
                        if ($currentNode->right === null) {
                            if ($parentNode === null) {
                                $this->root = $currentNode->left;
                            } else {
                                //if parent > current value, make current left child a child of parent
                                if ($currentNode->value < $parentNode->value) {
                                    $parentNode->left = $currentNode->left;
                                    //if parent < current value, make left child a right child of parent
                                } else {
                                    if ($currentNode->value > $parentNode->value) {
                                        $parentNode->right = $currentNode->left;
                                    }
                                }
                            }
                            //Option 2: Right child which doesnt have a left child
                        } else {
                            if ($currentNode->right->left === null) {
                                $currentNode->right->left = $currentNode->left;
                                if ($parentNode === null) {
                                    $this->root = $currentNode->right;
                                } else {
                                    //if parent > current, make right child of the left the parent
                                    if ($currentNode->value < $parentNode->value) {
                                        $parentNode->left = $currentNode->right;
                                        //if parent < current, make right child a right child of the parent
                                    } else {
                                        if ($currentNode->value > $parentNode->value) {
                                            $parentNode->right = $currentNode->right;
                                        }
                                    }
                                }
                                //Option 3: Right child that has a left child
                            } else {
                                //find the Right child's left most child
                                $leftmost = $currentNode->right->left;
                                $leftmostParent = $currentNode->right;
                                while ($leftmost->left !== null) {
                                    $leftmostParent = $leftmost;
                                    $leftmost = $leftmost->left;
                                }

                                //Parent's left subtree is now leftmost's right subtree
                                $leftmostParent->left = $leftmost->right;
                                $leftmost->left = $currentNode->left;
                                $leftmost->right = $currentNode->right;

                                if ($parentNode === null) {
                                    $this->root = $leftmost;
                                } else {
                                    if ($currentNode->value < $parentNode->value) {
                                        $parentNode->left = $leftmost;
                                    } else {
                                        if ($currentNode->value > $parentNode->value) {
                                            $parentNode->right = $leftmost;
                                        }
                                    }
                                }
                            }
                        }
                        return true;
                    }
                }
            }
        }
        return false;
    }
}

function traverse($node)
{
    $tree = new Node($node);
    $tree->left = $node->left === null ? null : traverse($node->left);
    $tree->right = $node->right === null ? null : traverse($node->right);
    return $tree;
}


$tree = new BinaryTree();

$tree->insert(3);
$tree->insert(2);
$tree->insert(245);
$tree->insert(212);
$tree->insert(21);
$tree->insert(1);

//var_dump($tree->root);
$js = json_encode(traverse($tree->root));
var_dump($js);
//var_dump(traverse($tree));
//var_dump($tree->root->value);
//var_dump($tree->find(212));
