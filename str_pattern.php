<?php
    
    /**
     * @param string $pattern
     * @param string $txt
     *
     * @return array
     */
    function strFindAll(string $pattern, string $txt): array
    {
        $patternLength = strlen($pattern);
        echo "Pattern length  : " . $patternLength ."\n";
        $textLength = strlen($txt);
        echo "Text length  : " . $textLength ."\n";
        $diff = $textLength - $patternLength;
        echo "Diffrence  : " . $diff ."\n";
        $positions = [];
        for ($i = 0; $i <= $diff; $i++) {
            for ($j = 0; $j < $patternLength; $j++) {
                if ($txt[$i + $j] != $pattern[$j]) {
                    break;
                }
            }
            if ($j == $patternLength) {
                $positions[] = $i;
            }
        }
        
        return $positions;
    }
    
    $txt     = "AABAACAADAABABBBAABAA";
    $pattern = "AABA";
    
    $matches = strFindAll($pattern, $txt);
    if ($matches) {
        foreach ($matches as $pos) {
            echo "Pattern found at index : " . $pos ."\n";
        }
    }
    
  