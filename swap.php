<?php
    
    //swap function vs refference
    function swap(&$a, &$b) {
        list($a, $b) = array($b, $a);
    }
    
    //swap values using third var
    $c=10;
    $d=20;
    $temp = $c;
    $c=$d;
    $d=$temp;