let cache = {};
function add10(n) {
    if (n in cache) {
        console.log('from Cache...');
        return cache[n];
    } else {
        console.log('Calculating...');
        cache[n] = n + 10;
        return cache[n];
    }
}


add10(10);
console.log(cache);
add10(10);
console.log(cache);
add10(30);
console.log(cache);
add10(30);
