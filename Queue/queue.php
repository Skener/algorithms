<?php
$queue = new SplQueue();
$queue->enqueue('A');
$queue->enqueue('B');
$queue->enqueue('C');

$queue->rewind();
while($queue->valid()){
    echo $queue->current(),"\n";
    $queue->next();
}

print_r($queue);
echo '<br>';
$queue->dequeue(); //remove first one
echo '<br>';
print_r($queue);
