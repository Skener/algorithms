<?php


class HashTable
{
    public $buckets;

    public function __construct($size)
    {
        $this->buckets[] = new SplFixedArray($size);
    }

    public function hash($key)
    {
        return (string)substr(md5($key), 0, 4);
    }

    public function set($key, $value)
    {
        $index = $this->hash($key);
        if (isset($this->buckets[$index]) === false) {
            $this->buckets[$index] = [];
        }

        if (isset($this->buckets[$index])) {
            $this->buckets[$index] = [$key => $value];
        }
        return true;
    }

    public function get($key)
    {
        $index = $this->hash($key);
        $currentBucket = $this->buckets[$index];
        $cur_bucket_length = count($currentBucket);
        if ($currentBucket) {
            foreach ($currentBucket as $k => $v) {
                if ($key === $k) {
                    return $currentBucket[$k];
                }
            }
        }
        return false;
    }
}

$myHashTable = new HashTable(2);
$myHashTable->set('grapes', 10000);
var_dump($myHashTable->get('grapes'));

