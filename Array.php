<?php

class MyArray
{

    public function __construct()
    {
        $this->length = 0;
        $this->items = [];
    }

    public function getItem($index)
    {
        return $this->items[$index];
    }

    public function addItem($item)
    {
        $this->items[$this->length] = $item;
        $this->length++;
        return $this->length;
    }

    public function removeLastItem()
    {
        unset($this->items[$this->length - 1]);
        $this->length--;
        return $this->length;
    }

    public function removeIndexItem($index)
    {
        $item = $this->items[$index];
        $this->shiftItems($index);

    }

    public function shiftItems($index)
    {
        for ($i = $index; $i < $this->length - 1; $i++) {
            $this->items[$i] = $this->items[$i + 1];
        }
        unset($this->items[$this->length - 1]);
        $this->length--;
    }

}

$ar = new MyArray();
$ar->addItem('one');
$ar->addItem('two');
$ar->addItem('three');
$ar->addItem('four');
$ar->removeLastItem();
$ar->removeIndexItem(1);
var_dump($ar->items);