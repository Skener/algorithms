<?php

// Memoization array approach
//$n='';
//$cache[$n] = [];
//function add10 ($n){
//    global $cache;
//    if (array_key_exists($n, $cache)){
//        echo 'from cache \n';
//        return $cache[$n];
//    }
//    else{
//        echo 'calculating \n';
//        $cache[$n] = $n+10;
//        return $cache[$n];
//    }
//}

//var_dump(add10(20));
//var_dump(add10(20));
//var_dump(add10(30));
//var_dump(add10(30));


// Memoization object approach
$cache = new StdClass();
function add10($n)
{
    global $cache;
    if (isset($cache->key) && $cache->key == $n) {
        echo "from cache \n";
        return $cache->val;
    } else {
        echo "calculating \n";
        $cache->key = $n;
        $cache->val = $n + 10;
        return $cache->val;
    }
}

var_dump(add10(1));
var_dump($cache);
var_dump(add10(1));
var_dump($cache);
var_dump(add10(2));
var_dump($cache);
var_dump(add10(2));
var_dump($cache);
